/*
 Copyright (C) 2009-2010 Bradley Clayton. All rights reserved.
 
 SQSimpleTableView can be downloaded from:
 https://bitbucket.org/dotb/sqsimpletableview
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 * Neither the name of the author nor the names of its contributors may be used
 to endorse or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "SQSimpleTableView.h"

@interface SQSimpleTableView ()
@property (nonatomic, retain) NSMutableDictionary *viewControllers;
@property (nonatomic, assign) id<UIScrollViewDelegate> clientDelegate;
@property (nonatomic, retain) NSTimer *intervalTimer;
- (void)removeAllSubViews;
@end

@implementation SQSimpleTableView

@synthesize dataSource;
@synthesize viewControllers;
@synthesize clientDelegate;
@synthesize intervalTimer;

#pragma mark -
#pragma mark NSObject Methods
- (id)initWithFrame:(CGRect)frame
{
	if ((self = [super initWithFrame:frame])) 
	{
        // Set up the scroll view
        [self setShowsHorizontalScrollIndicator:NO];
        [self setShowsVerticalScrollIndicator:NO];
        
		[self setViewControllers:[[NSMutableDictionary alloc] init]];
		[super setDelegate:self];
        [self setCurrentIndex:0];
        [self setSnapToPage:NO];
        [self setDecelerationRate:UIScrollViewDecelerationRateFast];
        [self setCanCancelContentTouches:YES];
	}
	return self;
}

#pragma mark -
#pragma mark UIScrollView Methods
- (void)setDelegate:(id<UIScrollViewDelegate >) _clientDelegate
{
    [self setClientDelegate:_clientDelegate];

}

#pragma mark -
#pragma mark UIView Methods
- (void)layoutSubviews
{
    [super layoutSubviews];

    if (dataSource)
	{
		NSInteger numberOfColumns = [dataSource numberOfColumnsForSQSimpleTableView:self];
		CGSize viewSize = [dataSource cellSizeForSQSimpleTableView:self];
		NSInteger padding = [dataSource paddingSizeForSQSimpleTableView:self];
		NSInteger cacheSize = [dataSource cacheSizeForSQSimpleTableView:self];
		CGPoint contentOffset = [self contentOffset];
        NSInteger startIndex =  (contentOffset.x / (viewSize.width + padding)) - cacheSize / 2;
		NSInteger viewsToLoad = ((contentOffset.x + self.bounds.size.width) / (viewSize.width + padding)) + cacheSize / 2;

        startIndex = (startIndex < 0) ? 0 : startIndex;
		viewsToLoad = (viewsToLoad > numberOfColumns) ? numberOfColumns : viewsToLoad;
        
		// Adjust the content size
		[self setContentSize:CGSizeMake(numberOfColumns * (padding * 2 + viewSize.width), self.bounds.size.height)];
		
		// Fetch and place the view objects
		for (NSInteger i = startIndex; i < viewsToLoad; i++)
		{
			UIViewController *viewControllerToPlace = [viewControllers objectForKey:[NSNumber numberWithInteger:i]];
			
			if (!viewControllerToPlace)
            {
				viewControllerToPlace = [dataSource SQSimpleTableView:self cellForRowAtIndex:i];
                [viewControllers setObject:viewControllerToPlace forKey:[NSNumber numberWithInteger:i]];
            }

            if (viewControllerToPlace && ![[viewControllerToPlace view] superview])
            {
                CGRect viewFrame = [[viewControllerToPlace view] frame];
                viewFrame.size = viewSize;
                viewFrame.origin.y = (self.frame.size.height / 2) - (viewFrame.size.height / 2);
                viewFrame.origin.x = padding + i * (padding + viewSize.width);
                [[viewControllerToPlace view] setFrame:viewFrame];
                
                [viewControllerToPlace viewWillAppear:NO];
                [self addSubview:[viewControllerToPlace view]];
                [viewControllerToPlace viewDidAppear:NO];
            }
		}
		
		// Remove unseen view objects
		for (NSInteger i = 0; i < startIndex; i++)
		{
			UIViewController *viewControllerToRemove = [viewControllers objectForKey:[NSNumber numberWithInteger:i]];
			if (viewControllerToRemove) 
			{
				[viewControllerToRemove viewWillDisappear:NO];
				[[viewControllerToRemove view] removeFromSuperview];
				[viewControllerToRemove viewDidDisappear:NO];
				[viewControllers removeObjectForKey:[NSNumber numberWithInteger:i]];
			}	
		}
		
		for (NSInteger i = viewsToLoad + 1; i < numberOfColumns; i++)
		{
			UIViewController *viewControllerToRemove = [viewControllers objectForKey:[NSNumber numberWithInteger:i]];
			if (viewControllerToRemove) 
			{
				[viewControllerToRemove viewWillDisappear:NO];
				[[viewControllerToRemove view] removeFromSuperview];
				[viewControllerToRemove viewDidDisappear:NO];
				[viewControllers removeObjectForKey:[NSNumber numberWithInteger:i]];
			}	
		}
		
	}
}

#pragma mark -
#pragma mark Public Methods
- (void)reloadData
{
	[self removeAllSubViews];
    [viewControllers removeAllObjects];
	[self setNeedsLayout];
}

- (void)reloadDataAfterIndex:(NSInteger) index
{
	[self removeAllSubViews];
    for (int i = index; i < [viewControllers count]; i++)
    {
        [viewControllers removeObjectForKey:[NSNumber numberWithInteger:i]];
    }
	[self setNeedsLayout];
}

- (void)scrollToIndex:(NSInteger) index animated:(BOOL) animated
{    
    if (index >= 0 && dataSource)
	{
        CGSize viewSize = [dataSource cellSizeForSQSimpleTableView:self];
        NSInteger padding = [dataSource paddingSizeForSQSimpleTableView:self];

        CGFloat scrollX = (index * (padding + viewSize.width)) - ((self.frame.size.width - viewSize.width) / 2) + padding;
        CGRect scrollRect = CGRectMake(scrollX, 0, self.bounds.size.width, self.bounds.size.height);
        [self scrollRectToVisible:scrollRect animated:animated];
    }
}

- (UIViewController *)viewControllerOnDisplayOrNil
{
    UIViewController *viewControllerOrNil = nil;
    
    if ([viewControllers objectForKey:[NSNumber numberWithInteger:_currentIndex]])
    {
        viewControllerOrNil = [viewControllers objectForKey:[NSNumber numberWithInteger:_currentIndex]];
    }
    else if (dataSource)
	{
		NSInteger numberOfColumns = [dataSource numberOfColumnsForSQSimpleTableView:self];
        if (numberOfColumns > _currentIndex)
        {
            viewControllerOrNil = [dataSource SQSimpleTableView:self cellForRowAtIndex:_currentIndex];
            [viewControllers setObject:viewControllerOrNil forKey:[NSNumber numberWithInteger:_currentIndex]];
        }
    }
    
    return viewControllerOrNil;
}

- (void)autoRotateAtInterval:(NSTimeInterval)interval
{
    [[self intervalTimer] invalidate];
    [self setIntervalTimer:[NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(scrollToNext) userInfo:nil repeats:YES]];
}

#pragma mark -
#pragma mark Private Methods
- (void)removeAllSubViews
{
	for (UIViewController *viewToRemove in [viewControllers allValues]) 
	{
		
		if (viewToRemove) 
		{
			[viewToRemove viewWillDisappear:NO];
			[[viewToRemove view] removeFromSuperview];
			[viewToRemove viewDidDisappear:NO];
		}
		
	}
    
	[viewControllers removeAllObjects];
}

- (void)scrollToNext
{
    if (self.currentIndex == [dataSource numberOfColumnsForSQSimpleTableView:self]-1)
    {
        [self scrollToIndex:0 animated:YES];
    }
    else
    {
        [self scrollToIndex:self.currentIndex+1 animated:YES];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (dataSource)
    {
		CGSize viewSize = [dataSource cellSizeForSQSimpleTableView:self];
		NSInteger padding = [dataSource paddingSizeForSQSimpleTableView:self];
		CGPoint contentOffset = [self contentOffset];
        
        NSInteger index = (contentOffset.x + padding + viewSize.width / 2) / (padding * 2 + viewSize.width);
        
        if (index != _currentIndex)
        {
            [self setCurrentIndex:(contentOffset.x + padding + viewSize.width / 2) / (padding * 2 + viewSize.width)];
            [self setCurrentIndex:(_currentIndex < 0) ? 0 : _currentIndex];

            UIViewController *viewControllerForDisplay = [self viewControllerOnDisplayOrNil];
            if (viewControllerForDisplay)
            {
                [viewControllerForDisplay viewWillAppear:YES];
            }

            if (dataSource)
            {
                [dataSource SQSimpleTableView:self didScrollToIndex:_currentIndex];
            }
            
            if (viewControllerForDisplay)
            {
                [viewControllerForDisplay viewDidAppear:YES];
            }
        }
    }
    
	[self setNeedsLayout];

    if (clientDelegate && [clientDelegate respondsToSelector:@selector(scrollViewDidScroll:)])
    {
        [clientDelegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (_snapToPage && dataSource)
	{
        CGSize viewSize = [dataSource cellSizeForSQSimpleTableView:self];
        NSInteger padding = [dataSource paddingSizeForSQSimpleTableView:self];
        targetContentOffset->x = (_currentIndex * (padding + viewSize.width)) - ((self.frame.size.width - viewSize.width) / 2) + padding;

        // Fix an issue where an x value of 0 does not move the scrollview
        if (targetContentOffset->x <= 0)
        {
            targetContentOffset->x = 1;
        }
    }
    
    if ([self intervalTimer])
    {
        [self autoRotateAtInterval:[[self intervalTimer] timeInterval]];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (clientDelegate && [clientDelegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)])
    {
        [clientDelegate scrollViewDidEndScrollingAnimation:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (clientDelegate && [clientDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)])
    {
        [clientDelegate scrollViewDidEndDecelerating:scrollView];
    }
}

@end
